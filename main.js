var express = require('express');
var app = express();
var compression = require('compression');
var config = require('./config/config.js');
// var db = require('./helpers/dbHelper.js');
var helmet = require('helmet');
var session = require('express-session');


var cities = require('./modules/cities/routes.js');

/**
 * Connect database
 */
// db.connect();






/**
 * Start server
 */
app.listen(config.port, function(){
    // console.log(`Example listening on port ${config.port}`);
    console.log('Server started on port ' + config.port);
});

app.use('/api/cities', cities);
app.use(helmet());
app.use(session(config.session));
app.use(compression());