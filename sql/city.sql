-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.18-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping data for table swallet.city: ~63 rows (approximately)
/*!40000 ALTER TABLE `city` DISABLE KEYS */;
INSERT INTO `city` (`id`, `code`, `name`, `type`) VALUES
	(1, '01', 'Thành phố Hà Nội', 'Thành phố Trung ương'),
	(2, '02', 'Tỉnh Hà Giang', 'Tỉnh'),
	(3, '04', 'Tỉnh Cao Bằng', 'Tỉnh'),
	(4, '06', 'Tỉnh Bắc Kạn', 'Tỉnh'),
	(5, '08', 'Tỉnh Tuyên Quang', 'Tỉnh'),
	(6, '10', 'Tỉnh Lào Cai', 'Tỉnh'),
	(7, '11', 'Tỉnh Điện Biên', 'Tỉnh'),
	(8, '12', 'Tỉnh Lai Châu', 'Tỉnh'),
	(9, '14', 'Tỉnh Sơn La', 'Tỉnh'),
	(10, '15', 'Tỉnh Yên Bái', 'Tỉnh'),
	(11, '17', 'Tỉnh Hoà Bình', 'Tỉnh'),
	(12, '19', 'Tỉnh Thái Nguyên', 'Tỉnh'),
	(13, '20', 'Tỉnh Lạng Sơn', 'Tỉnh'),
	(14, '22', 'Tỉnh Quảng Ninh', 'Tỉnh'),
	(15, '24', 'Tỉnh Bắc Giang', 'Tỉnh'),
	(16, '25', 'Tỉnh Phú Thọ', 'Tỉnh'),
	(17, '26', 'Tỉnh Vĩnh Phúc', 'Tỉnh'),
	(18, '27', 'Tỉnh Bắc Ninh', 'Tỉnh'),
	(19, '30', 'Tỉnh Hải Dương', 'Tỉnh'),
	(20, '31', 'Thành phố Hải Phòng', 'Thành phố Trung ương'),
	(21, '33', 'Tỉnh Hưng Yên', 'Tỉnh'),
	(22, '34', 'Tỉnh Thái Bình', 'Tỉnh'),
	(23, '35', 'Tỉnh Hà Nam', 'Tỉnh'),
	(24, '36', 'Tỉnh Nam Định', 'Tỉnh'),
	(25, '37', 'Tỉnh Ninh Bình', 'Tỉnh'),
	(26, '38', 'Tỉnh Thanh Hóa', 'Tỉnh'),
	(27, '40', 'Tỉnh Nghệ An', 'Tỉnh'),
	(28, '42', 'Tỉnh Hà Tĩnh', 'Tỉnh'),
	(29, '44', 'Tỉnh Quảng Bình', 'Tỉnh'),
	(30, '45', 'Tỉnh Quảng Trị', 'Tỉnh'),
	(31, '46', 'Tỉnh Thừa Thiên Huế', 'Tỉnh'),
	(32, '48', 'Thành phố Đà Nẵng', 'Thành phố Trung ương'),
	(33, '49', 'Tỉnh Quảng Nam', 'Tỉnh'),
	(34, '51', 'Tỉnh Quảng Ngãi', 'Tỉnh'),
	(35, '52', 'Tỉnh Bình Định', 'Tỉnh'),
	(36, '54', 'Tỉnh Phú Yên', 'Tỉnh'),
	(37, '56', 'Tỉnh Khánh Hòa', 'Tỉnh'),
	(38, '58', 'Tỉnh Ninh Thuận', 'Tỉnh'),
	(39, '60', 'Tỉnh Bình Thuận', 'Tỉnh'),
	(40, '62', 'Tỉnh Kon Tum', 'Tỉnh'),
	(41, '64', 'Tỉnh Gia Lai', 'Tỉnh'),
	(42, '66', 'Tỉnh Đắk Lắk', 'Tỉnh'),
	(43, '67', 'Tỉnh Đắk Nông', 'Tỉnh'),
	(44, '68', 'Tỉnh Lâm Đồng', 'Tỉnh'),
	(45, '70', 'Tỉnh Bình Phước', 'Tỉnh'),
	(46, '72', 'Tỉnh Tây Ninh', 'Tỉnh'),
	(47, '74', 'Tỉnh Bình Dương', 'Tỉnh'),
	(48, '75', 'Tỉnh Đồng Nai', 'Tỉnh'),
	(49, '77', 'Tỉnh Bà Rịa - Vũng Tàu', 'Tỉnh'),
	(50, '79', 'Thành phố Hồ Chí Minh', 'Thành phố Trung ương'),
	(51, '80', 'Tỉnh Long An', 'Tỉnh'),
	(52, '82', 'Tỉnh Tiền Giang', 'Tỉnh'),
	(53, '83', 'Tỉnh Bến Tre', 'Tỉnh'),
	(54, '84', 'Tỉnh Trà Vinh', 'Tỉnh'),
	(55, '86', 'Tỉnh Vĩnh Long', 'Tỉnh'),
	(56, '87', 'Tỉnh Đồng Tháp', 'Tỉnh'),
	(57, '89', 'Tỉnh An Giang', 'Tỉnh'),
	(58, '91', 'Tỉnh Kiên Giang', 'Tỉnh'),
	(59, '92', 'Thành phố Cần Thơ', 'Thành phố Trung ương'),
	(60, '93', 'Tỉnh Hậu Giang', 'Tỉnh'),
	(61, '94', 'Tỉnh Sóc Trăng', 'Tỉnh'),
	(62, '95', 'Tỉnh Bạc Liêu', 'Tỉnh'),
	(63, '96', 'Tỉnh Cà Mau', 'Tỉnh');
/*!40000 ALTER TABLE `city` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
