/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.5-10.2.6-MariaDB : Database - swallet
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`swallet` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `swallet`;

/*Table structure for table `wallet` */

DROP TABLE IF EXISTS `wallet`;

CREATE TABLE `wallet` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `bank_id` int(11) unsigned DEFAULT NULL,
  `currency_id` int(11) unsigned NOT NULL DEFAULT 0,
  `date_format` tinyint(1) unsigned NOT NULL COMMENT '1: dd-mm-yyyy, 2: mm-dd-yyyy, 3:yyyy-dd-mm, 4:yyyy-mm-dd, 5:dd/mm/yyyy, 6:mm/dd/yyyy, 7:yyyy/dd/mm, 8:yyyy/mm/dd',
  `view_mode` tinyint(1) unsigned NOT NULL DEFAULT 1 COMMENT '1:balance, 2:(in/out)flow',
  `name` varchar(20) NOT NULL,
  `type` tinyint(1) unsigned NOT NULL DEFAULT 1 COMMENT '1:cash,2:banking,3:zalopay,4:visa',
  `total_debt` float DEFAULT NULL,
  `total_loan` float DEFAULT NULL,
  `total_balance` float DEFAULT NULL,
  `total_available_balance` float DEFAULT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT 1 COMMENT '1:enabled,2:disabled,3:archived',
  `is_notification_enabled` tinyint(1) DEFAULT NULL,
  `is_excluded_from_total` tinyint(1) DEFAULT NULL,
  `created_by` int(11) unsigned NOT NULL,
  `last_edit_by` int(11) unsigned NOT NULL,
  `created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `status` (`status`),
  KEY `currency` (`currency_id`),
  CONSTRAINT `FK_wallet_currency` FOREIGN KEY (`currency_id`) REFERENCES `currency` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_wallet_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `wallet` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
