var express = require('express');
var router = express.Router();
var db = require('../../helpers/dbHelper.js');


router.get('/', function(req, res){
    db.query('SELECT * FROM city', function (error, results) {
        if (error) {
            throw error;
        } else {
            res.send(results);
        }
    });
});

module.exports = router;