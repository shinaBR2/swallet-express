var mysql = require('mysql');
var config = require('../config/config.js');

var pool = mysql.createPool(config.mysql);

module.exports = pool;