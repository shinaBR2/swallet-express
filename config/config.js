var config = {
    domain: '',
    port: 3010,
    mysql: {
        host     : 'localhost',
        port     : 3309,
        user     : 'root',
        password : 'S&s3:pro,ne.Je',
        database : 'swallet'
    },
    elastic: {
        host: ''
    },
    session: {
        cookie: {
            path: '/',
            httpOnly: true,
            secure: false,//Should highly recommended be true for only use HTTPs
            // expires: null,//should use maxAge instead
            maxAge: 3600000,//number in millisecond from now for calculate expire
            domain: 'localhost',
            // sameSite: true//should be review later            
        },
        /*genid: function(req) {
            //Should be careful to use this function
            // return genuuid() // use UUIDs for session IDs (require Unique)
        },*/
        // name: '',//name of session ID cookie to set in the response/read from request, default value is 'connect.sid'.
        // proxy: undefined, //true or false or undefined
        // resave: false, //research more
        // rolling: false, //research more
        // saveUninitialized: true, //research more
        secret: 'swallet_yiu&ASdd_213m123',
        // store: null, //Should be consider when production
        // unset: 'keep', //research more
    }
};

module.exports = config;